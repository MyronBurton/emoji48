//
//  GameBoard1.swift
//  emoji48
//
//  Created by Ryan Pendleton on 11/26/16.
//  Copyright © 2016 Ryan Pendleton. All rights reserved.
//

import UIKit

class GameBoard : UIView {
    var eBoxes = [[UILabel]]()
    var eBoxValues = [[Int]]()

    override func awakeFromNib() {
        self.backgroundColor = UIColor.darkGray
        for _ in 0...3 {
            var boxOBoxes : [UILabel] = []
            var eBoxValuesCong : [Int] = []
            for _ in 0...3 {
                let eBox = UILabel()
                boxOBoxes.append(eBox)
                eBoxValuesCong.append(0)
                self.addSubview(eBox)
            }
            eBoxes.append(boxOBoxes)
            eBoxValues.append(eBoxValuesCong)
        }

        let swipeRecognizerDown = UISwipeGestureRecognizer()
        self.addGestureRecognizer(swipeRecognizerDown)
        swipeRecognizerDown.addTarget(self, action: #selector(swipes))
        swipeRecognizerDown.direction = .down
        let swipeRecognizerUp = UISwipeGestureRecognizer()
        self.addGestureRecognizer(swipeRecognizerUp)
        swipeRecognizerUp.addTarget(self, action: #selector(swipes))
        swipeRecognizerUp.direction = .up
        let swipeRecognizerRight = UISwipeGestureRecognizer()
        self.addGestureRecognizer(swipeRecognizerRight)
        swipeRecognizerRight.addTarget(self, action: #selector(swipes))
        swipeRecognizerRight.direction = .right
        let swipeRecognizerLeft = UISwipeGestureRecognizer()
        self.addGestureRecognizer(swipeRecognizerLeft)
        swipeRecognizerLeft.addTarget(self, action: #selector(swipes))
        swipeRecognizerLeft.direction = .left

        self.newTileGenerator()
        self.newTileGenerator()
    }

    override func layoutSubviews() {
        let padding = self.frame.width * 0.02
        let eBoxSize = (self.frame.width - padding * 5) / 4
        for eColumn in 0...3 {
            for eRow in 0...3 {
                let eBox = eBoxes[eColumn][eRow]
                eBox.frame.size.width = eBoxSize
                eBox.frame.size.height = eBoxSize
                eBox.frame.origin.x = CGFloat(eColumn + 1) * padding + CGFloat(eColumn) * eBoxSize
                eBox.frame.origin.y = CGFloat(eRow + 1) * padding + CGFloat(eRow) * eBoxSize
            }
        }
    }

    func swipes(swipeRecognizer: UISwipeGestureRecognizer) {
        switch swipeRecognizer.direction {
        case UISwipeGestureRecognizerDirection.down:
            NSLog("iwasswipeddown")
        case UISwipeGestureRecognizerDirection.up:
            NSLog("iwasswipedup")
        case UISwipeGestureRecognizerDirection.right:
            NSLog("iwasswipedright")
        case UISwipeGestureRecognizerDirection.left:
            NSLog("iwasswipedleft")
        default :
            NSLog("whattheheck")
            abort()
        }
    }
    
    func addTiles(_ eColumn1: Int, _ eColumn2: Int, _ eRow1: Int, _ eRow2: Int) -> Bool {
        if eBoxValues[eColumn1][eRow1] == eBoxValues[eColumn2][eRow2] {
            eBoxValues[eColumn1][eRow1] = 0
            eBoxValues[eColumn2][eRow2] *= 2
            return true
        }
        else {
            var algo = 3
            switch eColumn1 {
            case 0:     //column 0
                while algo >= 0 {
                    if algo == 0 {
                        return false
                    }
                    else {
                        if eBoxValues[eColumn1 - algo][eRow1] == 0 {
                            return true
                        }
                        else {
                            algo -= 1
                        }
                    }
                }
            case 1:     //column 1
                while algo >= 0 {
                    if algo == 1 {
                        algo -= 1
                    }
                    else {
                        if eBoxValues[eColumn1 - algo][eRow1] == 0 {
                            return true
                        }
                        else {
                            algo -= 1
                        }
                    }
                }
                return false
            case 2:     //column 2
                while algo >= 0 {
                    if algo == 2 {
                        algo -= 1
                    }
                    else {
                        if eBoxValues[eColumn1 - algo][eRow1] == 0 {
                            return true
                        }
                        else {
                            algo -= 1
                        }
                    }
                }
                return false
            case 3:     //column 3
                while algo >= 0 {
                    if algo == 3 {
                        algo -= 1
                    }
                    else {
                        if eBoxValues[eColumn1 - algo][eRow1] == 0 {
                            return true
                        }
                        else {
                            algo -= 1
                        }
                    }
                }
            default {
            }
            }
            switch eRow1 {
            case 0:     //row 0
                while algo >= 0 {
                    if algo == 0 {
                        return false
                    }
                    else {
                        if eBoxValues[eColumn1][eRow1 - algo] == 0 {
                            return true
                        }
                        else {
                            algo -= 1
                        }
                    }
                }
            case 1:     //row 1
                while algo >= 0 {
                    if algo == 1 {
                        algo -= 1
                    }
                    else {
                        if eBoxValues[eColumn1][eRow1 - algo] == 0 {
                            return true
                        }
                        else {
                            algo -= 1
                        }
                    }
                }
            return false
            case 2:     //row 2
                while algo >= 0 {
                    if algo == 2 {
                        algo -= 1
                    }
                    else {
                        if eBoxValues[eColumn1][eRow1 - algo] == 0 {
                            return true
                        }
                        else {
                            algo -= 1
                        }
                    }
                }
            return false
            case 3:     //row 3
                while algo >= 0 {
                    if algo == 3 {
                        algo -= 1
                    }
                    else {
                        if eBoxValues[eColumn1][eRow1 - algo] == 0 {
                            return true
                        }
                        else {
                            algo -= 1
                        }
                    }
                }
            default {
            }
            }
        return false
        }
    }

    func newTileGenerator() {
        var emptyCount = 0
        for eColumn in 0...3 {
            for eRow in 0...3 {
                if eBoxValues[eColumn][eRow] == 0 {
                    emptyCount += 1
                }
            }
        }
        var randomTile = arc4random_uniform(UInt32(emptyCount))
        for eColumn in 0...3 {
            for eRow in 0...3 {
                if eBoxValues[eColumn][eRow] == 0 {
                    if randomTile == 0 {
                        if arc4random_uniform(10) == 4 {
                            eBoxValues[eColumn][eRow] = 4
                        }
                        else {
                            eBoxValues[eColumn][eRow] = 2
                        }
                        self.updateBoard()
                        return
                    }
                    randomTile -= 1
                }
            }
        }
    }
    func updateBoard() {
        for eColumn in 0...3 {
            for eRow in 0...3 {
                let eBox = eBoxes[eColumn][eRow]
                var eBright = 1.2
//                switch eBoxValues[eColumn][eRow] {
//                case 2:
//                case 4:
//                    eBox.backgroundColor = UIColor(hue: 0.775, saturation: 0.30, brightness: 0.65, alpha: 1.0)
//                case 8:
//                    eBox.backgroundColor = UIColor(hue: 0.775, saturation: 0.40, brightness: 0.60, alpha: 1.0)
//                case 16:
//                    eBox.backgroundColor = UIColor(hue: 0.775, saturation: 0.50, brightness: 0.55, alpha: 1.0)
//                case 32:
//                    eBox.backgroundColor = UIColor(hue: 0.775, saturation: 0.60, brightness: 0.50, alpha: 1.0)
//                case 64:
//                    eBox.backgroundColor = UIColor(hue: 0.775, saturation: 0.70, brightness: 0.45, alpha: 1.0)
//                default:
//                    eBox.backgroundColor = UIColor.lightGray
//                }
                if eBoxValues[eColumn][eRow] == 0 {
                    eBright = 1.2
                }
                else {
                    let count = convertPower(eBoxValues[eColumn][eRow])
                    eBright = -(Double(count) / 17) + 1.2
                }
                eBox.backgroundColor = UIColor(hue: 0.775, saturation: 0.20, brightness: CGFloat(eBright), alpha: 1.0)
                eBox.text = boxString(eBoxValues: eBoxValues[eColumn][eRow])
            }
        }
    }
    func convertPower(_ power: Int) -> Int {
        var count = 1
        var power1 = power
        while power1 != 2 {
            power1 /= 2
            count += 1
        }
        return count
    }
    func boxString(eBoxValues: Int) -> String {
        if eBoxValues > 0 {
            return String(eBoxValues)
        }
        else {
            return ""
        }
    }
}
