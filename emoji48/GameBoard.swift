//
//  GameBoard.swift
//  emoji48
//
//  Created by Ryan Pendleton on 11/26/16.
//  Copyright © 2016 Ryan Pendleton. All rights reserved.
//

import UIKit

class GameBoard1 : UIView {
    var eBoxes = [[UIView]]()

    override func awakeFromNib() {
        super.awakeFromNib()

        for _ in 0...3 {
            var array : [UIView] = []

            for _ in 0...3 {
                let eBox = UIView()
                eBox.backgroundColor = UIColor.red
                self.addSubview(eBox)
                array.append(eBox)
            }

            eBoxes.append(array)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        let eBoxPadding = self.frame.width * 0.02
        let eBoxSize = (self.frame.width - eBoxPadding * 5) / 4

        for eColumn in 0...3 {
            for eRow in 0...3 {
                let eBox = eBoxes[eColumn][eRow]

                eBox.frame.origin.x = CGFloat(eColumn + 1) * eBoxPadding + CGFloat(eColumn) * eBoxSize
                eBox.frame.origin.y = CGFloat(eRow + 1) * eBoxPadding + CGFloat(eRow) * eBoxSize
                eBox.frame.size.width = eBoxSize
                eBox.frame.size.height = eBoxSize
            }
        }

    }
}
